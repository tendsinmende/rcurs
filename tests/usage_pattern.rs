use rcurs::*;
use rcurs::cell::RcuCell;

#[test]
fn short_use(){
    
    let rcu = Rcu::new(4);
    assert!(*rcu.read() == 4);

    //With bound setter
    {
	let mut setter = rcu.write();
	assert!(*rcu.read() == 4);
	*setter = 10;
	assert!(*rcu.read() == 4)
    }
    assert!(*rcu.read() == 10, "10=={} failed", *rcu.read());

    //With unbound
    *rcu.write() = 12;
    assert!(*rcu.read() == 12);
    
}

#[test]
fn single_threaded_cell(){
    let cell = RcuCell::new(10);

    let cell_cp = cell.clone();
    let cell_cp2 = cell.clone();
    {
	let scoped_cp = cell_cp2.clone();
    }
}

#[test]
fn shared_cell(){
    const NUM_THREAD: usize = 10;
    let mut handle = Vec::with_capacity(NUM_THREAD);

    let shared = RcuCell::new(10);
    
    for _i in 0..NUM_THREAD{
	let si = shared.clone();
	let hdl = std::thread::spawn(move ||{
	    let start = std::time::Instant::now();

	    while start.elapsed() < std::time::Duration::from_secs(2){
		let local_cell = si.clone();
		{
		    let scoped_cell = local_cell.clone();
		}
	    }
	});

	handle.push(hdl);
    }

    std::thread::sleep(std::time::Duration::from_secs(2));
    for h in handle{
	h.join().unwrap()
    }
}

#[test]
fn reads(){
    
    let rcu = Rcu::new(10);
    let long_reader = rcu.read();
    {
	let reader = rcu.read();
	let a = *reader;
    }
    
}

#[test]
fn random_multi_thread(){
    
    const NUM_THREAD: usize = 10;
    let mut handle = Vec::with_capacity(NUM_THREAD);

    let shared = std::sync::Arc::new(Rcu::new(10 as u32));
    
    for _i in 0..NUM_THREAD{
	let si = shared.clone();
	let hdl = std::thread::spawn(move ||{
	    let start = std::time::Instant::now();

	    while start.elapsed() < std::time::Duration::from_secs(2){
		*si.write() = *si.read() + 1;
	    }
	});

	handle.push(hdl);
    }

    std::thread::sleep(std::time::Duration::from_secs(2));
    for h in handle{
	h.join().unwrap()
    }
    
}
