use std::{sync::Arc, time::Duration};

use rcurs::Rcu;

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/
#[derive(Clone)]
struct Element(usize, usize);

fn main(){
    let rcu = Arc::new(Rcu::new(Element(10, 0)));
    let mut threads = Vec::new();
    for i in 0..4{
	let local_rcu = rcu.clone();
	let new_thread = std::thread::spawn(move||{
	    let start = std::time::Instant::now();
	    let mut n = 0;
	    //loop{
	    //for i in 0..4096{
	    while start.elapsed() < std::time::Duration::from_secs(5){
		//println!("Current value: {}", *local_rcu.read());
		let mut writer = local_rcu.write();
                let value = local_rcu.read();
		
                writer.0 = value.0.wrapping_add(value.1);
                writer.1 = i;

		n+=1;
	    }
	    println!("Thread {} end after {} iterations", i, n);
	});
	threads.push(new_thread);
	
    }

    
    for (i, t) in threads.into_iter().enumerate(){
	t.join().expect("Failed to join");
	println!("Joined {}", i);
    }
}
