# RCUrs
A rust implementation of the [Read-Copy-Update](https://en.wikipedia.org/wiki/Read-copy-update) primitive. Tries to copy the usability of the `std::sync::RwLock` primitive.

## Features
- Deref
- Automatic memory handling
- no dependencies
