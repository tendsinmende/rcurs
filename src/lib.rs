/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
/* Useful tags: Free Tibet, Free Hong Kong, Free Xinjiang, Free Taiwan*/

use std::{ops::{Deref, DerefMut}, sync::{Arc, atomic::{AtomicBool, AtomicPtr, Ordering, fence}}};



pub struct RcuReader<T: Clone + Send>{
    value: T
}

impl<T: Clone + Send> Deref for RcuReader<T>{
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.value
    }
}


pub struct RcuWriter<T: Clone + Send>{
    value: Option<T>,
    ptr: Arc<AtomicPtr<T>>,
    ptr_lock: Arc<AtomicBool>,
}

impl<T: Clone + Send> Deref for RcuWriter<T>{
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.value.as_ref().unwrap()
    }
}

impl<T: Clone + Send> DerefMut for RcuWriter<T>{
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.value.as_mut().unwrap()
    }
}

impl<T: Clone + Send> Drop for RcuWriter<T>{
    fn drop(&mut self) {
        //on drop we update the pointer of the rcu, but before that we take it and contruct the initial
        //Arc from it, to let the drop implementation of Arc do its thing

        let new_arc = Arc::new(self.value.take().unwrap()); //TODO fix me with a take?
        let new_arc_ptr = Arc::into_raw(new_arc) as *mut T;

        while self.ptr_lock.compare_and_swap(false, true, Ordering::Acquire){
            std::hint::spin_loop()
        }

        //Swap pointer
        let old_ptr = self.ptr.swap(new_arc_ptr, Ordering::Release);

        self.ptr_lock.store(false, Ordering::Release);
        
        //Create the arc from the old ptr and drop it
        let old_arc = unsafe{Arc::from_raw(old_ptr)};
        drop(old_arc);
    }
}

pub struct Rcu<T: Clone + Send>{
    //points to the current most recent memory location
    chainging_ptr: Arc<AtomicBool>,
    memptr: Arc<AtomicPtr<T>>
}

impl<T: Clone + Send> Rcu<T>{
    pub fn new(value: T) -> Self{
        let arc = Arc::new(value);
        Rcu{
            chainging_ptr: Arc::new(AtomicBool::new(false)),
            memptr: Arc::new(AtomicPtr::new(Arc::into_raw(arc) as *mut _))
        }
    }

    pub fn read(&self) -> RcuReader<T>{
        //Deref pointer, and create new arc, clone that arc, and leak again.
        unsafe{

            //busy wait
            while self.chainging_ptr.compare_and_swap(false, true, Ordering::Acquire){
                std::hint::spin_loop()
            }
            
            let inner_val = Arc::from_raw(self.memptr.load(Ordering::Acquire)).clone();
            let value = (*inner_val).clone();
            //Now leak the pointer again
            let _ = Arc::into_raw(inner_val);

            //Release
            self.chainging_ptr.store(false, Ordering::Release);
            
            //Create reader, our clone must have copied the ptr and therefore incremented the strong count.
            RcuReader{
                value
            }
        }
    }

    pub fn write(&self) -> RcuWriter<T>{
        RcuWriter{
            ptr: self.memptr.clone(),
            value: Some((*self.read()).clone()),
            ptr_lock: self.chainging_ptr.clone()
        }
    }
}

impl<T: Clone + Send> Drop for Rcu<T>{
    fn drop(&mut self) {
        //Contruct inner arc of current value and drop it
        let val = unsafe{ Arc::from_raw(self.memptr.load(Ordering::SeqCst))};
        fence(Ordering::SeqCst);
        drop(val)
    }
}
